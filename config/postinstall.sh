#!/bin/bash

set -eou pipefail

cat <<EOF
systemd-rollback has been installed as a systemd service 👋👋

🔖 To start/stop systemd-rollback 
    $ sudo systemctl start systemd-rollback
    $ sudo systemctl stop systemd-rollback

🔖 To enable/disable systemd-rollback starting automatically on boot
    $ sudo systemctl enable systemd-rollback
    $ sudo systemctl disable systemd-rollback

🔖 To restart systemd-rollback
    $ sudo systemctl restart systemd-rollback

🔖 To view systemd-rollback logs
    $ journalctl -f -u systemd-rollback
EOF

# sudo systemctl start systemd-rollback
# sudo systemctl enable systemd-rollback


# Automatically added by dh_installsystemd/13.2
if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ] ; then
        # This will only remove masks created by d-s-h on package removal.
        deb-systemd-helper unmask 'systemd-rollback.service' >/dev/null || true

        # was-enabled defaults to true, so new installations run enable.
        if deb-systemd-helper --quiet was-enabled 'systemd-rollback.service'; then
                # Enables the unit on first installation, creates new
                # symlinks on upgrades if the unit file has changed.
                deb-systemd-helper enable 'systemd-rollback.service' >/dev/null || true
        else
                # Update the statefile to add new symlinks (if any), which need to be
                # cleaned up on purge. Also remove old symlinks.
                deb-systemd-helper update-state 'systemd-rollback.service' >/dev/null || true
        fi
fi
# End automatically added section
# Automatically added by dh_installsystemd/13.2
# if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ] ; then
#         if [ -d /run/systemd/system ]; then
#                 systemctl --system daemon-reload >/dev/null || true
#                 if [ -n "$2" ]; then
#                         _dh_action=reload
#                 else
#                         _dh_action=start
#                 fi
#                 deb-systemd-invoke $_dh_action 'systemd-rollback.service' >/dev/null || true
#         fi
# fi
# # End automatically added section

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ] ; then
        if [ -d /run/systemd/system ]; then
                systemctl --system daemon-reload >/dev/null || true
                if [ -n "$2" ]; then
                        deb-systemd-invoke try-restart 'systemd-rollback.service' >/dev/null || true
                fi
        fi
fi

##

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ] ; then
        # This will only remove masks created by d-s-h on package removal.
        deb-systemd-helper unmask 'systemd-rollback.socket' >/dev/null || true

        # was-enabled defaults to true, so new installations run enable.
        if deb-systemd-helper --quiet was-enabled 'systemd-rollback.socket'; then
                # Enables the unit on first installation, creates new
                # symlinks on upgrades if the unit file has changed.
                deb-systemd-helper enable 'systemd-rollback.socket' >/dev/null || true
        else
                # Update the statefile to add new symlinks (if any), which need to be
                # cleaned up on purge. Also remove old symlinks.
                deb-systemd-helper update-state 'systemd-rollback.socket' >/dev/null || true
        fi
fi

##

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ] ; then
        if [ -d /run/systemd/system ]; then
                systemctl --system daemon-reload >/dev/null || true
                if [ -n "$2" ]; then
                        deb-systemd-invoke try-restart 'systemd-rollback.socket' >/dev/null || true
                fi
        fi
fi

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ]; then
        if [ -z "${DEBCONF_RECONFIGURE:-}" ] && ! invoke-rc.d systemd-rollback status > /dev/null 2>&1 ; then
                invoke-rc.d systemd-rollback start || :
        fi
fi