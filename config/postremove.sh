#!/bin/bash

set -euo pipefail

if [ -d /run/systemd/system ]; then
        systemctl --system daemon-reload >/dev/null || true
fi
# End automatically added section
# Automatically added by dh_installsystemd/12.10ubuntu1
if [ "$1" = "remove" ]; then
        if [ -x "/usr/bin/deb-systemd-helper" ]; then
                deb-systemd-helper mask 'systemd-rollback.socket' >/dev/null || true
        fi
fi

if [ "$1" = "purge" ]; then
        if [ -x "/usr/bin/deb-systemd-helper" ]; then
                deb-systemd-helper purge 'systemd-rollback.socket' >/dev/null || true
                deb-systemd-helper unmask 'systemd-rollback.socket' >/dev/null || true
        fi
fi

# End automatically added section
# Automatically added by dh_installsystemd/12.10ubuntu1
if [ -d /run/systemd/system ]; then
        systemctl --system daemon-reload >/dev/null || true
fi
# End automatically added section
# Automatically added by dh_installsystemd/12.10ubuntu1
if [ "$1" = "remove" ]; then
        if [ -x "/usr/bin/deb-systemd-helper" ]; then
                deb-systemd-helper mask 'systemd-rollback.service' >/dev/null || true
        fi
fi

if [ "$1" = "purge" ]; then
        if [ -x "/usr/bin/deb-systemd-helper" ]; then
                deb-systemd-helper purge 'systemd-rollback.service' >/dev/null || true
                deb-systemd-helper unmask 'systemd-rollback.service' >/dev/null || true
        fi
fi