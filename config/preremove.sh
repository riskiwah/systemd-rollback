#!/bin/bash

set -eou pipefail

cat <<EOF
Whoops..
System will be reload ↪️
EOF

# sudo systemctl daemon-reload
# sudo systemctl reload systemd-rollback

# systemctl stop systemd-rollback
# systemctl disable systemd-rollback
# rm /etc/systemd/system/systemd-rollback.service
# systemctl daemon-reload
# systemctl reset-failed

#######

#!/bin/sh

# Automatically added by dh_installsystemd/13.2
if [ -d /run/systemd/system ] && [ "$1" = remove ]; then
        deb-systemd-invoke stop 'systemd-rollback.service' >/dev/null || true
fi
# End automatically added section

if [ -d /run/systemd/system ] && [ "$1" = remove ]; then
        deb-systemd-invoke stop 'systemd-rollback.socket' >/dev/null || true
fi

if [ "$1" = remove ]; then
        invoke-rc.d systemd-rollback stop
fi